# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import requests

# Create your views here.
def index(request):
    url = 'https://www.google.com/url?q=http://api.openweathermap.org/data/2.5/weather?q%3Dpune%26APPID%3D3dff7f89ae428e1f43b0cbeef5c452c8&sa=D&source=hangouts&ust=1536749777457000&usg=AFQjCNFAicJNdtqE4TN93H_KGgEhJQ4VFw'
    city = 'Las Vegas'
    weather = requests.get(url.format(city)).json()
    return render(request, 'weather/index.html')
